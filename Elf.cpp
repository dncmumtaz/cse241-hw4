#include "Elf.h"

Elf::Elf() : Creature() {
	setType("Elf");
}

Elf::Elf(int tempS, int tempH) : Creature(tempS, tempH)
 {
	setType("Elf");
}

int Elf::getDamage() const {

	int damage;
	Creature elf(getStrength(), getHit());



	elf.setType(getSpecies());
	damage = elf.getDamage();

	if ((rand() % 10) == 0) {
		cout << endl <<  damage << "extra damage points!" << endl;
		damage = damage * 2;
	}

	return damage;
}

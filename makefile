MAIN = main.cpp Creature.cpp Demon.cpp Elf.cpp Human.cpp Balrog.cpp Cyberdemon.cpp 

CPP = g++

VERS = -w --std=c++11

OUT = output

all :	$(MAIN)
		$(CPP) $(MAIN) $(VERS) -o $(OUT)
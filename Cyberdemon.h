#ifndef CYBERDEMON_H_
#define CYBERDEMON_H_


#include "Demon.h"


class Cyberdemon : public Demon {
public:
	Cyberdemon();
	Cyberdemon(int tempS, int tempH);
};

#endif /*CYBERDEMON_H_*/
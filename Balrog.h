#ifndef BALROG_H_
#define BALROG_H_

#include "Demon.h"


class Balrog : public Demon {
public:
	Balrog();
	Balrog(int tempS, int tempH);

	int getDamage() const;
};


#endif /*BALROG_H_*/
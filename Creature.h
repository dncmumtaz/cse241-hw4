#ifndef CREATURE_H_
#define CREATURE_H_

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>


using namespace std;

class Creature
{
private:
	int strength;
	int hit;
	string type;
	
public:
	Creature();
	Creature(int tempS, int tempH);

	int getDamage() const;
	int getStrength() const;
	int getHit() const;
	string getSpecies() const;

	void setStrength(int tempS);
	void setHit(int tempH);
	void setType(string tempT);

};





#endif /*CREATURE_H_*/



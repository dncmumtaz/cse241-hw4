#include "Human.h"
#include "Demon.h"
#include "Balrog.h"
#include "Cyberdemon.h"
#include "Elf.h"



int main()
{

	srand(time(NULL));
	
	
	for (int i = 0; i < 3; ++i)
	{
		int damage = 0;
		Human mertcan(12, 9);
		Cyberdemon tugay(5, 8);
		Balrog semih(13, 17);
		Elf yilmaz(6, 6);


		damage = yilmaz.getHit() - semih.getDamage();
		yilmaz.setHit(damage);

		if (yilmaz.getHit() < 0)
			cout << endl << "Elf lost";

		damage = mertcan.getHit() - tugay.getDamage();
		mertcan.setHit(damage);
		if (mertcan.getHit() < 0)
			cout << endl << "Human lost  " << endl; 


		damage = tugay.getHit() - yilmaz.getDamage();
		tugay.setHit(damage);
		if (tugay.getHit() < 0)
			cout << endl << "tugay lost!" << endl;
		cout << endl << endl << "-------------------------------------------" << endl;
	}

	cout << endl << endl;

	return 0;
}
#include "Human.h"

Human::Human() : Creature() {
	setStrength(10);
	setHit(10);
	setType("Human");
}

Human::Human(int tempS, int tempH) : Creature(tempS, tempH)
{
	setStrength(tempS);
	setHit(tempH);
	setType("Human");
}

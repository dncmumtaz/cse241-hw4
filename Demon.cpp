#include "Demon.h"

Demon::Demon() : Creature() 
{
	setType("Demon");
}

Demon::Demon(int tempS, int tempH) : Creature(tempS, tempH)
 {
	setType("Demon");
}

int Demon::getDamage() const {

	int damage;
	Creature demon(getStrength(), getHit());

	demon.setType(getSpecies());

	damage = demon.getDamage();

	if ((rand() % 100) < 5) {
		damage = damage + 50;
		cout << endl << damage << "extra damage" << endl;
	}

	return damage;
}
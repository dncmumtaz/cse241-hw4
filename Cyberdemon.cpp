#include "Cyberdemon.h"

Cyberdemon::Cyberdemon() : Demon()
{
	setType("Cyberdemon");
}

Cyberdemon::Cyberdemon(int tempS, int tempH) : Demon(tempS, tempH)
{
	setType("Cyberdemon");
}
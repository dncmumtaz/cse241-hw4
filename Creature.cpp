#include "Creature.h"

Creature::Creature() {
	strength = 0;
	hit = 0;
	type = "";
}

Creature::Creature(int tempS, int tempH) {
	strength = tempS;
	hit = tempH;
	type = "";
}

int Creature::getDamage() const {

	int damage;


	damage = (rand() % getStrength()) + 1;
	cout << endl << getSpecies() << " attacks for " << damage << " points!";

	return damage;
}

int Creature::getStrength() const {
	return strength;
}

int Creature::getHit() const {
	return hit;
}

string Creature::getSpecies() const {
	return type;
}

void Creature::setStrength(int tempS) {
	strength = tempS;
	return;
}

void Creature::setHit(int tempH) {
	hit = tempH;
	return;
}

void Creature::setType(string tempT) {
	type = tempT;
	return;
}
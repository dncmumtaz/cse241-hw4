#ifndef DEMON_H_
#define DEMON_H_

#include "Creature.h"


class Demon : public Creature {
public:
	Demon();
	Demon(int tempS, int tempH);

	int getDamage() const;
};


#endif /*DEMON_H_*/
#ifndef HUMAN_H_
#define HUMAN_H_


#include "Creature.h"

class Human : public Creature {
public:
	Human();
	Human(int tempS, int tempH);
};

#endif /*HUMAN_H_*/
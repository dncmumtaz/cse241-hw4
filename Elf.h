#ifndef ELF_H_
#define ELF_H_


#include "Creature.h"

class Elf : public Creature {
public:
	Elf();
	Elf(int tempS, int tempH);

	int getDamage() const;
};



#endif /*ELF_H_*/